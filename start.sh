#!/bin/bash

set -eux
sed -e "s/\*\*REPLACE\*\*/\//" .htaccess > .htaccess

/usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i GetSimpleCMS &

while ! supervisorctl status apache2 | grep -q RUNNING; do sleep 1; done # wait for apache2 to start

echo "HOSTNAME : $HOSTNAME" >&2
#echo "APP_ORIGIN : $APP_ORIGIN" >&2

URL_BASE="http://localhost"
APP_URL="https://$HOSTNAME/"
echo "URL_BASE : $URL_BASE" >&2

curl -L $URL_BASE/admin/ 


curl -L -X POST --data "continue=true" $URL_BASE/admin/setup.php

curl -L -X POST --data "siteurl=$URL_BASE/&lang=en_US&sitename=Untitled&user=admin&email=admin@localhost.com&submitted=true" $URL_BASE/admin/setup.php
curl -L $URL_BASE/admin/support.php?updated=2

NEWPASS=`echo -n "pass1234" | openssl dgst -sha1 -hmac`
NEWPASS=`echo ${NEWPASS#*=} | tr -d [:space:]`
echo "NEWPASS $NEWPASS" >&2
sed -e "s&\(<PWD>\).*\(</PWD>\)&\1$NEWPASS\2&" -i /app/code/data/users/admin.xml 
sed -e "s&\(<SITEURL><\!\[CDATA\[\).*\(\]\]></SITEURL>\)&\1$APP_URL\2&" -i /app/code/data/other/website.xml

wait
