FROM girish/base:0.1.0
MAINTAINER Abhishek Patil <abhishek@zeroth.me>

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L http://get-simple.info/dreamhost-pickup/GetSimpleCMS_3.3.5.zip > GetSimpleCMS_3.3.5.zip && unzip GetSimpleCMS_3.3.5.zip && mv /app/code/GetSimpleCMS-3.3.5/*  /app/code
RUN rm -rf /app/code/GetSimpleCMS-3.3.5 && rm /app/code/GetSimpleCMS_3.3.5.zip
RUN chmod 777 /app/code/data /app/code/backups
RUN mv temp.htaccess .htaccess
RUN mv temp.gsconfig.php gsconfig.php
RUN chown -R www-data.www-data /app/code

ADD start.sh /app/code/start.sh
RUN chmod +x /app/code/start.sh

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -i 's/upload_max_filesize = .*/upload_max_filesize = 65M/' /etc/php5/apache2/php.ini
ADD apache2-getsimplecms.conf /etc/apache2/sites-available/getsimplecms.conf
RUN ln -sf /etc/apache2/sites-available/getsimplecms.conf /etc/apache2/sites-enabled/getsimplecms.conf
RUN a2enmod php5
RUN a2enmod rewrite

# supervisor
ADD supervisor-apache2.conf /etc/supervisor/conf.d/apache2.conf

EXPOSE 80

CMD [ "/app/code/start.sh" ]
